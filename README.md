![logo](https://github.com/Sciss/ScalaColliderAppTemplate/raw/main/src/main/resources/doe/jane/myscalacolliderapp/icon.png)

# ScalaCollider Template App

## statement

This is a template to make a [ScalaCollider](https://sciss.de/ScalaCollider) based application.
It assumes that SuperCollider (scsynth) is independently installed on the system. The source code
uses [Scala 3](https://docs.scala-lang.org/scala3/book/introduction.html).

The project is made available under the GNU Affero General Public License v3 (or newer), the
same as the most recent version of ScalaCollider.

Simply open `MyScalaColliderApp.scala` and add your own code.

To run via sbt, simply execute `sbt run`. To build a standalone application, run `sbt assembly`.
The resulting jar can be executed via `java -jar MyScalaColliderApp.jar`.

We have included the `sbt` script from [sbt-extras project](https://github.com/dwijnand/sbt-extras) 
which uses a BSD-style license, so if you do
not want to install sbt, you can just execute `./sbt run`.
