lazy val root = project.in(file("."))
  .settings(
    name           := "MyScalaColliderApp",
    version        := "0.1.0-SNAPSHOT",
    organization   := "doe.jane",
    scalaVersion   := "3.0.0",
    licenses       := Seq("AGPL v3+" -> url("http://www.gnu.org/licenses/agpl-3.0.txt")),
    libraryDependencies ++= Seq(
      "de.sciss" %% "scalacollider" % "2.6.4",
      "org.scala-lang.modules" %% "scala-swing" % "3.0.0"
    ),
    // bundling
    assembly / assemblyJarName := s"${name.value}.jar",
    assembly / target := baseDirectory.value,
  )
